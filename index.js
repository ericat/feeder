'use strict';

var server = require('./server');

server.start(function() {
  console.log('Server running at:', server.info.uri);
});

process.once('uncaughtException', function(err) {
  console.error('uncaughtException: ', err.stack || err);
  console.trace();
  process.exit(1);
});
