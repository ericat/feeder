var path = require('path');
var webpack = require('webpack');

module.exports = {
  entry: path.resolve(__dirname, 'client', 'root.jsx'),
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'public'),
  },
  externals: {
    'cheerio': 'window',
    'react/lib/ExecutionEnvironment': true,
    'react/lib/ReactContext': true,
  },
  module: {
    loaders: [
      {test: require.resolve('react-addons-perf'), loader: 'expose?Perf'},
      {test: /\.css$/, loader: 'style-loader!css-loader'},
      {
        test: /\.html$/,
        loader: 'file?name=[name].[ext]'
      },
      {
        test: /\.jsx?$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true
        },
      },
    ]
  },
  plugins: [
   new webpack.DefinePlugin({
    'process.env.NODE_ENV':  JSON.stringify(process.env.NODE_ENV),
  }),
  ],
  resolve: {
    extensions: ['', '.js', '.jsx', '.json', '.css'],
    modulesDirectories: [
      'node_modules',
      'client'
    ]
  },
};

