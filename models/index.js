var Feed = require('./feed');
var Link = require('./link');

module.exports = {
  Link: Link,
  Feed: Feed,
};
