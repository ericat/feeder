'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var LinkSchema = new Schema({
  title: {type: String, required: true},
  author: {type: String, required: true},
  content: {type: String, required: true},
  preview: {type: String},
  date: {type: Date, required: true},
  url: {type: String, required: true},
  content: {type: String, required: true},
  feedTitle: {type: String},
  feedUrl: {type: String},
  starred: {type: Boolean, default: false},
});

mongoose.model('links', LinkSchema);

module.exports = mongoose.model('links');
