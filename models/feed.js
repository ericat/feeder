var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var FeedSchema = new Schema({
  name: {type: String, required: true},
  url: {type: String, required: true},
  _links: [{type: Schema.Types.ObjectId, ref: 'links'}]
});

mongoose.model('feeds', FeedSchema);

module.exports = mongoose.model('feeds');
