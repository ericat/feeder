var mongoose = require('mongoose');
var config = require('../config');
var helpers = require('../server/helpers');
var Feed = require('../models/feed');
var Link = require('../models/Link');
var links = require('../links.json');

mongoose.connect(config.server.db);

function _dropFeedCb(cb) {
  return function(err) {
    if (err) { return cb(err); }
    Link.remove({}, _dropLinksCb(cb));
  };
}

function _dropLinksCb(cb) {
  return function(err) {
    if (err) { return cb(err); }

    var formattedLinks = helpers.parseFeed(links);
    var feed = helpers.buildFeedFromMeta(formattedLinks[0]);

    Feed.create(feed, _createFeedCb(formattedLinks, cb));
  };
}

function _createFeedCb(formattedLinks, cb) {
  return function(err, savedFeed) {
    if (err) { return cb(err); }
    var length = formattedLinks.length;

    formattedLinks.map(_createLinksCb(length, savedFeed, cb));
  };
}

function _createLinksCb(length, savedFeed, cb) {
  return function(formattedLink) {
    Link.create(formattedLink, function(err, link) {
      if (err) { return cb(err); }

      savedFeed._links.push(link._id);
      length--;

      if (length == 0) {
        return savedFeed.save(_saveFeedCb(savedFeed, cb));
      }
    });
  };
}

function _saveFeedCb(feed, cb) {
  return function(err) {
    if (err) { return cb(err); }
    Link.find({}, _updateLinkCb(feed, cb));
  };
}

function _updateLinkCb(savedFeed, cb) {
  return function(err, links) {
    if (err || !links) { return cb(err); }
    var id = links.pop()._id;

    Link.update({_id: id}, {starred: true}, function(err) {
      if (err) { return cb(err); }
      var selectedProps = ['_id', 'name', 'url'];
      var clone = JSON.parse(JSON.stringify(savedFeed, selectedProps));
      cb(null, clone);
    });
  };
}

module.exports = function(cb) {
  function seed() {
    console.log('***seeding database: development');

    Feed.remove({}, _dropFeedCb(cb));
  }

  seed(links, function(err) {
    if (err) { return cb(err); };
    cb(null);
  });
};
