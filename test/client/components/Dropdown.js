import React from 'react';
import { mount, render, shallow } from 'enzyme';
import chai from 'chai';
import sinon from 'sinon';
import chaiEnzyme from 'chai-enzyme';
import Dropdown from '../../../client/components/Dropdown';

chai.use(chaiEnzyme());
const expect = chai.expect;

describe('<Dropdown /> component', () => {
  let onSelect;
  let onChange;
  let feeds;

  beforeEach(() => {
    onSelect = sinon.spy();
    feeds = [{
      _id: '576fb9de948217fe47dbd6dc',
      name: 'WIRED'
    }];
  });

  it('renders a React element', () => {
    expect(React.isValidElement(<Dropdown
      data={feeds}
      onSelect={onSelect}
    />)).to.equal(true);
  });

  it('fires an onChange event', () => {
    const rendered = mount(<Dropdown
      data={feeds}
      onSelect={onSelect}
    />);

    rendered.find('select').simulate('change', {
      target: {value: 'WIRED'}
    });

    expect(onSelect.calledOnce).to.equal(true);
  });
});
