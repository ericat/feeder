import React from 'react';
import { mount, render, shallow } from 'enzyme';
import chai from 'chai';
import sinon from 'sinon';
import chaiEnzyme from 'chai-enzyme';
import FilterLink from '../../../client/components/FilterLink';

chai.use(chaiEnzyme());
const expect = chai.expect;

describe('<FilterLink /> component', () => {
  let viewMode;
  let currentViewMode;
  let onClick;
  let style;
  let title;
  let wrapper;

  beforeEach(() => {
    viewMode = '';
    currentViewMode = '';
    onClick = sinon.spy();
    style = 'classNameString';
    title = 'Show all';

    wrapper = shallow(<FilterLink
      viewMode={viewMode}
      currentViewMode={currentViewMode}
      onClick={onClick}
      style={style}
      title={title}>
        Starred
      </FilterLink>);
  });

  it('displays a span if viewMode == currentViewMode', () => {
      expect(wrapper.find('span')).to.have.length(1);
  });

  it('displays a link otherwise', () => {
    wrapper.setProps({currentViewMode: 'SHOW_STARRED'});

    wrapper.find('a').at(0).simulate('click');
    expect(onClick.calledOnce).to.equal(true);
  });
});
