import React from 'react';
import { mount, render, shallow } from 'enzyme';
import chai from 'chai';
import sinon from 'sinon';
import chaiEnzyme from 'chai-enzyme';
import Links from '../../../client/components/Links';

chai.use(chaiEnzyme());
const expect = chai.expect;

describe('<Links /> component', () => {
  let links;
  let onClick;
  let viewMode = '';
  let selectedItem = '123';

  beforeEach(() => {
    onClick = sinon.spy();
    links = [{
      _id: '123',
      title: 'A beautiful article',
      author: 'Erica Salvaneschi',
      content: 'The content',
      preview: 'The preview',
      date: new Date().toString(),
      url: 'http://example/article/',
      feedTitle: 'Mashable',
      feedUrl: 'http://mashable.com/stories/',
      starred: false,
    }];
  });

  it('renders a React element', () => {
    expect(React.isValidElement(<Links
      data={links}
      expanded={false}
      onClick={onClick}
      viewMode={viewMode}
      selectedItem={selectedItem}
    />)).to.equal(true);
  });

  it('is expanded when expanded is true', () => {
    const rendered = mount(<Links
      data={links}
      expanded={true}
      onClick={onClick}
      viewMode={viewMode}
      selectedItem={selectedItem}
    />);

    expect(rendered.find('p.Links-preview')).to.have.length(0);
    expect(rendered.find('p.Links-full')).to.have.length(1);
  });
});

