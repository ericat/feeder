import React from 'react';
import { mount, render, shallow } from 'enzyme';
import chai from 'chai';
import chaiEnzyme from 'chai-enzyme';
import thunk from 'redux-thunk';
import sinon from 'sinon';
import configureStore from 'redux-mock-store';
import App from '../../../client/containers/App';
import Dropdown from '../../../client/components/Dropdown';
import Links from '../../../client/components/Links';
import AddFeedModal from '../../../client/components/AddFeedModal';
import FilterLink from '../../../client/components/FilterLink';

chai.use(chaiEnzyme());
const expect = chai.expect;

const middlewares = [thunk];
const mockStore = configureStore(middlewares);

describe('<App /> container', () => {
  let link;
  let store;
  let storeStateMock;
  let defaultProps;

  beforeEach(() => {
    link = {
      _id: '123',
      title: 'A beautiful article',
      author: 'Erica Salvaneschi',
      content: 'The content',
      preview: 'The preview',
      date: new Date().toString(),
      url: 'http://example/article/',
      feedTitle: 'Mashable',
      feedUrl: 'http://mashable.com/stories/',
      starred: false,
      expanded: false,
    };
    storeStateMock = {
      feedsReducer: {
        feeds: [{_id: '123', name: 'Mashable'}],
        links: [link],
        showModal: false,
        selectedFeed: '',
        selectedItem: '',
        viewMode: '',
        showModal: false,
        isExpanded: false,
        error: ''
      }
    };

    store = mockStore(storeStateMock);
  });

  it('renders a React element', () => {
    expect(React.isValidElement(<App />)).to.equal(true);
  });

  it('is rendered with links props', () => {
    const wrapper = shallow(<App />, {context: {store: store}}).shallow();

    expect(wrapper.props().links).to.be.defined;
  });

  it.skip('calls componentWillMount', () => {
    const rendered = mount(<App />, {context: {store: store}});
    const willMount = sinon.spy();
    rendered.componentWillMount = willMount;
    expect(willMount.callCount).to.equal(1);
  });

  it('contains show all and show starred filters', () => {
    const rendered = mount(<App />, {context: {store: store}});
    expect(rendered.find('a.App-showStarred')).to.have.length(1);
    expect(rendered.find('span.App-showAll')).to.have.length(1);
  });

  it('contains the Dropdown component', () => {
    const rendered = mount(<App />, {context: {store: store}});

    expect(rendered.find(Dropdown)).to.have.length(1);
    expect(rendered.find(Dropdown)).to.have.prop('data');
  });

  it('shows the AddFeedModal component', () => {
    const rendered = mount(<App />, {context: {store: store}});
    expect(rendered.find('a').at(1)).to.have.text('+');
    expect(rendered.find('a').at(0)).to.have.prop('onClick');
    expect(rendered.find(AddFeedModal)).to.have.length(1);
  });

  it('contains two FilterLink component', () => {
    const rendered = mount(<App />, {context: {store: store}});
    expect(rendered.find(FilterLink)).to.have.length(2);
    expect(rendered.find('a').at(0)).to.have.prop('onClick');
  });

  it('contains the Links component', () => {
    const rendered = mount(<App />, {context: {store: store}});
    expect(rendered.find(Links)).to.have.length(1);
    expect(rendered.find(Links)).to.have.prop('data');
    expect(rendered.find(Links)).to.have.prop('viewMode');
  });
});
