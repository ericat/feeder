import React from 'react';
import { mount, render, shallow } from 'enzyme';
import chai from 'chai';
import sinon from 'sinon';
import chaiEnzyme from 'chai-enzyme';
import AddFeedModal from '../../../client/components/AddFeedModal';

chai.use(chaiEnzyme());
const expect = chai.expect;

describe('<AddFeedModal /> component', () => {
  let onSubmit;
  let onClose;

  beforeEach(() => {
    onSubmit = sinon.spy();
    onClose = sinon.spy();
  });

  it('renders a React element', () => {
    expect(React.isValidElement(<AddFeedModal
                                isVisible={false}
                                onClose={onClose}
                                onSubmit={onSubmit}
                                />)).to.equal(true);
  });

  it('is initially hidden', () => {
    const rendered = mount(<AddFeedModal
                           isVisible={false}
                           onClose={onClose}
                           onSubmit={onSubmit}
                           />);
    expect(rendered.find(AddFeedModal)).to.have.style('display', 'none');

    rendered.setProps({isVisible: true});
    expect(rendered.find(AddFeedModal)).to.have.style('display', 'block');
  });

  it('contains a form with an input', () => {
    const rendered = mount(<AddFeedModal
                           isVisible={true}
                           onClose={onClose}
                           onSubmit={onSubmit}
                           />);
    expect(rendered.find('form input')).to.have.length(1);
  });

  it('fires onSubmit on submit', () => {
    const e = {preventDefault: sinon.spy()};
    const rendered = mount(<AddFeedModal
                           isVisible={true}
                           onClose={onClose}
                           onSubmit={onSubmit}
                           />);
    rendered.setState({urlValue: 'http://www.example.com'});
    rendered.find('button').at(0).simulate('click');
    expect(onSubmit.calledOnce).to.equal(true);
  });

  it('only submits valid URLs', () => {
    const rendered = mount(<AddFeedModal
                           isVisible={true}
                           onClose={onClose}
                           onSubmit={onSubmit}
                           />);
    rendered.find('input');
    rendered.simulate('focus');
    rendered.simulate('change', {target: {value: ''}});
    rendered.find('form').simulate('submit');

    expect(onSubmit.notCalled).to.equal(true);
    expect(rendered.state().validationError)
      .to.equal('Not a valid URL');
  });

  it('fires onClose on close', () => {
    const e = {preventDefault: sinon.spy()};
    const rendered = mount(<AddFeedModal
                           isVisible={true}
                           onClose={onClose}
                           onSubmit={onSubmit}
                           />);
    rendered.find('a').at(0).simulate('click');
    expect(onClose.calledOnce).to.equal(true);
  });
});

