import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import nock from 'nock';
import expect from 'expect';
import sinon from 'sinon';
import * as actions from '../../../client/actions/feeds';
import * as types from '../../../client/constants/actionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Feeds actions', () => {
  var link;

  beforeEach(function() {
    link = {
      title: 'Sharing code between native and web: React and Relay',
      author: 'Erica Salvaneschi',
    };
  });

  afterEach(() => {
    nock.cleanAll();
  });

  it('creates LOAD_FEED', () => {
    nock('http://localhost:8080')
      .get('/v1/feed/123')
      .reply(200, {body: {links: [link]}});

    const expectedActions = [{
      type: types.LOAD_FEED,
      body: {links: [link]},
    }];

    const store = mockStore({links: []});
    const dispatch = sinon.spy(store, 'dispatch');

    store.dispatch(actions.loadFeed('123'));
    expect(dispatch.calledWith({type: 'LOAD_FEED', links: [link]}));
  });
});
