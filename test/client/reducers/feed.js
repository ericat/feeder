import expect from 'expect';
import { Map, List, toJS, fromJS } from 'immutable';
import feedsReducer from '../../../client/reducers/feeds';
import * as types from '../../../client/constants/actionTypes';

describe('Feeds reducer', () => {
  let initialState;
  let links;
  let link;
  let feed;

  beforeEach(function() {
    initialState = new Map({
      feeds: new List(),
      isExpanded: false,
      links: new List(),
      selectedFeed: '',
      selectedItem: '',
      showModal: false,
      viewMode: '',
      error: ''
    });

    link = {
      title: 'Sharing code between native and web: React and Relay',
      author: 'Erica Salvaneschi',
      id: '123',
    };

    feed = {
      _id: '345',
      name: 'Mashable',
    };
  });

  it('should return the initial state', () => {
    expect(
      feedsReducer(initialState, {})
    ).toEqual(initialState);
  });

  it('should handle LOAD_DROPDOWN', () => {
    const feeds = [feed];

    const expected = feedsReducer(initialState, {
      type: types.LOAD_DROPDOWN,
      payload: feeds
    });

    const nextState = fromJS({
      feeds: [feed],
      isExpanded: false,
      links: new List(),
      selectedFeed: '',
      selectedItem: '',
      showModal: false,
      viewMode: '',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle LOAD_FEED', () => {
    links = [link];

    const expected = feedsReducer(initialState, {
      type: types.LOAD_FEED,
      payload: links
    });

    const nextState = fromJS({
      feeds: new List(),
      isExpanded: false,
      links: [link],
      selectedFeed: '',
      selectedItem: '',
      showModal: false,
      viewMode: '',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle CREATE_FEED', () => {
    const newFeed = {
      _id: '1234',
      name: 'Wired',
    };

    const expected = feedsReducer(initialState, {
      type: types.CREATE_FEED,
      payload: newFeed,
    });

    const nextState = fromJS({
      feeds: [newFeed],
      isExpanded: false,
      links: new List(),
      selectedFeed: 'Wired',
      selectedItem: '',
      showModal: false,
      viewMode: '',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle LOAD_ERROR', () => {
    const expected = feedsReducer(initialState, {
      type: types.LOAD_ERROR,
      error: 'this is an error'
    });

    const nextState = fromJS({
      feeds: new List(),
      isExpanded: false,
      links: new List(),
      selectedFeed: '',
      selectedItem: '',
      showModal: false,
      viewMode: '',
      error: 'this is an error'
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle SET_FEED', () => {
    const expected = feedsReducer(initialState, {
      type: types.SET_FEED,
      text: 'Mashable'
    });

    const nextState = fromJS({
      feeds: new List(),
      isExpanded: false,
      links: new List(),
      selectedFeed: 'Mashable',
      selectedItem: '',
      showModal: false,
      viewMode: '',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle SET_VIEWMODE', () => {
    const expected = feedsReducer(initialState, {
      type: types.SET_VIEWMODE,
      text: 'SHOW_STARRED'
    });

    const nextState = fromJS({
      feeds: new List(),
      isExpanded: false,
      links: new List(),
      selectedFeed: '',
      selectedItem: '',
      showModal: false,
      viewMode: 'SHOW_STARRED',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle SHOW_MODAL', () => {
    const expected = feedsReducer(initialState, {
      type: types.SHOW_MODAL,
      text: true,
    });

    const nextState = fromJS({
      feeds: new List(),
      isExpanded: false,
      links: new List(),
      selectedFeed: '',
      selectedItem: '',
      showModal: true,
      viewMode: '',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle SET_EXPANDED', () => {
    const expected = feedsReducer(initialState, {
      type: types.SET_EXPANDED,
      text: true,
    });

    const nextState = fromJS({
      feeds: new List(),
      isExpanded: true,
      links: new List(),
      selectedFeed: '',
      selectedItem: '',
      showModal: false,
      viewMode: '',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });

  it('should handle SET_SELECTED_ITEM', () => {
    const expected = feedsReducer(initialState, {
      type: types.SET_SELECTED_ITEM,
      text: '123',
    });

    const nextState = fromJS({
      feeds: new List(),
      isExpanded: false,
      links: new List(),
      selectedFeed: '',
      selectedItem: '123',
      showModal: false,
      viewMode: '',
      error: ''
    });

    expect(nextState.toJS()).toEqual(expected);
  });
});
