'use strict';
var Lab = require('lab');
var lab = exports.lab = Lab.script();
var describe = lab.experiment;
var it = lab.it;
var Code = require('code');
var expect = Code.expect;
var helpers = require('../../server/helpers');
var links = require('../../links.json');

describe('Helpers: parseFeed should', function() {
  it('parse the feed of links', function(done) {
    var feed = helpers.parseFeed(links);

    expect(feed[0]).to.include('title');
    expect(feed[0]).to.include('author');
    expect(feed[0]).to.not.include('image');
    done();
  });
});
