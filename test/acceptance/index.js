import assert from 'assert';
import server from '../../server/';

const Browser = require('zombie');
Browser.localhost('example.com', 8080);

describe('Acceptance tests - ', () => {
  describe('Feeds', () => {
    let browser;
    before((done) => {
      browser = new Browser();
      server.start(() => {
        console.log('Zombie server running at:', server.info.uri);
        done();
      });
    });

    after((done) => {
      server.stop();
      done();
    });

    it('should have a title', (done) => {
      browser.visit('/', {debug: true}, (err) => {
        if (err) throw err;
        browser.assert.text('title', 'Feed aggregator');
        done();
      });
    });
  });
});
