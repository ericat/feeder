'use strict';
var Lab = require('lab');
var lab = exports.lab = Lab.script();
var describe = lab.experiment;
var before = lab.before;
var after = lab.after;
var beforeEach = lab.beforeEach;
var it = lab.it;
var Code = require('code');
var expect = Code.expect;
var assert = require('assert');
var server = require('../../server');
var LinkModel = require('../../models').Link;
var FeedModel = require('../../models').Feed;
var links = require('../../links.json');

describe('Unit tests - ', function() {
  describe('Feeds should', function() {
    var Feed;
    var Link;
    var feedName;
    var linkId;

    beforeEach(function(done) {
      Feed = server.methods.feed;
      var linkFixture = {
        title: 'A title',
        author: 'An author',
        content: 'A content',
        url: 'http://example.com/article',
        date: new Date(),
        starred: false,
        feedUrl: 'http://example.com',
        feedTitle: 'TechCrunch',
      };

      LinkModel.create(linkFixture, function(err, link) {
        if (err) { throw err; };
        assert.equal(link.title, linkFixture.title);
        assert.equal(link.starred, linkFixture.starred);
        linkId = link._id;
        done();
      });
    });

    after(function(done) {
      FeedModel.find({}).remove().exec(function(err) {
        if (err) { throw err; };
        LinkModel.find({}).remove().exec(function(err) {
          if (err) { throw err; };
          done();
        });
      });
    });

    it('create a feed with links', function(done) {
      var body = {url: 'http://techcrunch.com/rss'};
      var feedFixture = {
        name: 'TechCrunch',
        url: 'https://techcrunch.com'
      };

      function fetchLinks(body, cb) {
        return cb(null, links);
      }

      Feed.create(body, {fetchLinks: fetchLinks}, function(err, feed) {
        if (err) { throw err; };
        expect(feed).to.not.include('_links');
        assert(feed.name, feedFixture.name);
        assert(feed.url, feedFixture.url);
        feedName = feed.name;
        done();
      });
    });

    it('retrieve a list of links by feed name', function(done) {
      Feed.findByName(feedName, function(err, links) {
        if (err) { throw err; };
        expect(links).to.be.an.array();
        done();
      });
    });

    it('retrieve all feed names', function(done) {
      Feed.findNames(function(err, feeds) {
        assert(feeds[0].toObject().name, feedName);
        if (err) { throw err; };
        done();
      });
    });

    it('retrieve all links', function(done) {
      Link = server.methods.link;
      Link.findAll(function(err, links) {
        expect(links).to.be.an.array();
        if (err) { throw err; };
        done();
      });
    });

    it('patch a link', function(done) {
      var options = {starred: true};
      Link.upsert(linkId, options, function(err, link) {
        if (err) { throw err; };
        expect(err).to.equal(null);
        expect(link).to.be.an.object();
        expect(link.starred).to.equal(true);
        done();
      });
    });
  });
});
