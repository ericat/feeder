'use strict';
var Lab = require('lab');
var lab = exports.lab = Lab.script();
var describe = lab.experiment;
var after = lab.after;
var it = lab.it;
var Code = require('code');
var expect = Code.expect;
var assert = require('assert');
var server = require('../../server');
var Feed = require('../../models').Feed;
var Link = require('../../models').Link;

describe('Integration tests - ', function()  {
  var feedName;
  var linkId;

  describe('Feed API should', function() {
    it('create a feed of articles', function(done) {
      var rssUrl = {url: 'http://blog.yld.io/rss/'};

      server.inject({
        method: 'POST',
        url: '/v1/feed',
        payload: rssUrl,
      }, function(res) {
        expect(res.statusCode).to.be.equal(200);
        var feed = JSON.parse(res.payload);

        expect(feed).to.not.include('_links');
        feedName = feed.name;
        done();
      });
    });

    it('find a feed by name', function(done) {
      server.inject({
        method: 'GET',
        url: '/v1/feeds/' + feedName,
      }, function(res) {
        expect(res.statusCode).to.be.equal(200);
        var feed = JSON.parse(res.payload);

        expect(feed[0]).to.include('title');
        expect(feed[0]).to.include('author');
        linkId = feed[0]._id;
        done();
      });
    });

    it('find feed names and ids', function(done) {
      server.inject({
        method: 'GET',
        url: '/v1/feeds/names',
      }, function(res) {
        expect(res.statusCode).to.be.equal(200);
        var feedNames = JSON.parse(res.payload);

        expect(feedNames[0]).to.include('name');
        done();
      });
    });

    it('return all links', function(done) {
      server.inject({
        method: 'GET',
        url: '/v1/links',
      }, function(res) {
        expect(res.statusCode).to.be.equal(200);
        var links = JSON.parse(res.payload);

        expect(links).to.be.an.array();
        done();
      });
    });

    it('patch a single link', function(done) {
      var options = {
        starred: true,
      };

      server.inject({
        method: 'PUT',
        url: '/v1/links/' + linkId,
        payload: options,
      }, function(res) {
        expect(res.statusCode).to.be.equal(200);
        var link = JSON.parse(res.payload);

        expect(link.starred).to.equal(true);
        done();
      });
    });
  });
});
