'use strict';
var server = require('../server');
var loadMethods = require('../server/methods');
var helpers = require('../server/helpers');

module.exports = {
  create: function(request, reply) {
    server.methods.feed.create(request.payload, {}, onCreate);

    function onCreate(err, feed) {
      if (err) { return reply(err); };
      reply(feed).code(200);
    }
  },

  findByName: function(request, reply) {
    server.methods.feed.findByName(request.params.name, onFindByName);

    function onFindByName(err, feed) {
      if (err) { return reply(err); };
      reply(feed).code(200);
    }
  },

  findNames: function(request, reply) {
    server.methods.feed.findNames(onFindNames);

    function onFindNames(err, feedNames) {
      if (err) { return reply(err); };
      reply(feedNames).code(200);
    }
  },
};

