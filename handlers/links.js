'use strict';
var server = require('../server');
var loadMethods = require('../server/methods');

module.exports = {
  findAll: function(request, reply) {
    server.methods.link.findAll(onFindAll);

    function onFindAll(err, links) {
      if (err) { return reply(err); };
      reply(links).code(200);
    }
  },

  upsert: function(request, reply) {
    server.methods.link.upsert(request.params.id,
      request.payload,
      onUpsert
    );

    function onUpsert(err, link) {
      if (err) { return reply(err); };
      reply(link).code(200);
    }
  },
};
