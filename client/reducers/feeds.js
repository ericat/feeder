import { Map, List, toJS, fromJS } from 'immutable';
import * as types from '../constants/ActionTypes';

let initialState = new Map({
  feeds: new List(),
  isExpanded: false,
  links: new List(),
  selectedFeed: '',
  selectedItem: '',
  showModal: false,
  viewMode: '',
  error: '',
});

export default function feedsReducer(state = initialState, action) {
  switch (action.type) {
    case 'LOAD_FEED':
      return fromJS(state)
        .set('links', action.payload)
        .toJS();
    case 'CREATE_FEED':
      return fromJS(state)
        .update('selectedFeed', (value) => {
          value = action.payload.name;
          return value;
        })
        .set('feeds', fromJS(state)
        .get('feeds')
        .push(action.payload))
        .toJS();
    case 'SET_FEED':
      return fromJS(state)
        .set('selectedFeed', action.text)
        .toJS();
    case 'SHOW_MODAL':
      return fromJS(state)
        .set('showModal', action.text)
        .toJS();
    case 'LOAD_DROPDOWN':
      return fromJS(state)
        .set('feeds', action.payload)
        .toJS();
    case 'LOAD_ERROR':
      return fromJS(state)
        .update('error', (value) => {
          value = action.error;
          return value;
        })
        .toJS();
    case 'SET_VIEWMODE':
      return fromJS(state)
        .update('viewMode', (value) => {
          value = action.text;
          return value;
        })
        .toJS();
    case 'SET_EXPANDED':
      return fromJS(state)
        .update('isExpanded', (value) => {
          value = action.text;
          return value;
        })
        .toJS();
    case 'SET_SELECTED_ITEM':
      return fromJS(state)
        .update('selectedItem', (value) => {
          value = action.text;
          return value;
        })
        .toJS();
    default:
      return state;
  }
}
