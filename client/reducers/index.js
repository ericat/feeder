import { combineReducers } from 'redux';
import feedsReducer from './feeds';

const rootReducer = combineReducers({
  feedsReducer,
});

export default rootReducer;
