import fetch from 'isomorphic-fetch';
import * as types from '../constants/actionTypes';

function checkStatus(res) {
  if (res.status >= 200 && res.status < 300) {
    return res;
  } else {
    const error = new Error(res.statusText);
    error.res = res;
    throw error;
  }
}

export function loadDefaults() {
  return (dispatch, getState) => {
    const url = '/v1/feeds/names';

    fetch(url, {
      credentials: 'same-origin',
    })
    .then(checkStatus)
    .then(res => res.json())
    .then(feeds => {
      dispatch({
        type: 'LOAD_DROPDOWN',
        payload: feeds,
      });
      dispatch(loadFeed());
    }).catch(err =>
      dispatch({
        type: 'LOAD_ERROR',
        error: `${err} : Request failed`,
      })
    );
  };
}

export function loadFeed() {
  return (dispatch, getState) => {
    const url = '/v1/links';

    fetch(url, {
      credentials: 'same-origin',
    })
    .then(checkStatus)
    .then(res => res.json())
    .then(links => {
      dispatch({
        type: 'LOAD_FEED',
        payload: links,
      });
    }).catch(err =>
      dispatch({
        type: 'LOAD_ERROR',
        error: `${err} : Cannot load feed`,
      })
    );
  };
}

export function setFeed(text) {
  return {
    type: 'SET_FEED',
    text,
  };
}

export function showAddModal(text) {
  return {
    type: 'SHOW_MODAL',
    text: text,
  };
}

export function addFeed(feedUrl) {
  return (dispatch, getState) => {
    const url = '/v1/feed';

    fetch(url, {
      method: 'POST',
      credentials: 'same-origin',
      body: JSON.stringify({ url: feedUrl }),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    })
    .then(checkStatus)
    .then(res => res.json())
    .then(res => {
      const newFeed = {
        _id: res._id,
        name: res.name,
      };
      dispatch({
        type: 'CREATE_FEED',
        payload: newFeed,
      });
      dispatch(loadFeed());
      dispatch(showAddModal(false));
    }).catch(err =>
      dispatch({
        type: 'LOAD_ERROR',
        error: `${err} : Cannot create feed`,
      })
    );
  };
}

export function setViewMode(text) {
  return {
    type: 'SET_VIEWMODE',
    text,
  };
}

export function setExpanded(text) {
  return {
    type: 'SET_EXPANDED',
    text,
  };
}

export function setSelectedItem(text) {
  return {
    type: 'SET_SELECTED_ITEM',
    text,
  };
}
