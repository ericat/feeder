import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import { Provider } from 'react-redux';
import { Router, Route, browserHistory } from 'react-router';
import configureStore from './store/configure-store';
import App from './containers/App';
import * as actions from 'actions/feeds';

const store = configureStore();

render(
  <Provider store={store}>
    <Router history={browserHistory}>
      <Route path='/(:fiter)' component={App} />
    </Router>
  </Provider>,
  document.getElementById('root')
);
