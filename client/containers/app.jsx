import 'babel-polyfill';
import React from 'react';
import { render, findDOMNode } from 'react-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as actions from 'actions/feeds';
import Dropdown from '../components/Dropdown';
import AddFeedModal from '../components/AddFeedModal';
import Links from '../components/Links';
import FilterLink from '../components/FilterLink';
import './app.css';

const cssPrefixer = require('../components/utils/css-prefixer');
const Perf = require('react-addons-perf');

@cssPrefixer()
class App extends React.Component {
  constructor(props) {
    super(props);

    this._handleClick = this._handleClick.bind(this);
    this._handleCreateFeed = this._handleCreateFeed.bind(this);
    this._handleSelect = this._handleSelect.bind(this);
    this._handleSetViewMode = this._handleSetViewMode.bind(this);
  }

  componentWillMount() {
    const { loadDefaults } = this.props;
    loadDefaults();
  }

  _handleClick(id) {
    const { isExpanded, setExpanded, setSelectedItem } = this.props;
    setSelectedItem(id);
    setExpanded(!isExpanded);
  }

  _handleSelect(feed) {
    const { setFeed, setViewMode } = this.props;

    if (!feed) {
      return setViewMode('');
    }

    setFeed(feed);
    setViewMode('SHOW_SELECTED');
  }

  _handleSetViewMode(viewMode) {
    const { setViewMode } = this.props;

    this.refs.feedDropdown.refs.feed.value = '';
    setViewMode(viewMode);
  }

  _handleCreateFeed(url) {
    const { addFeed } = this.props;
    addFeed(url);
    this._handleSetViewMode('');
  }

  render() {
    const {
      links,
      feeds,
      selectedFeed,
      selectedItem,
      setFeed,
      showAddModal,
      showModal,
      addFeed,
      loadFeed,
      setViewMode,
      viewMode,
      setExpanded,
      isExpanded,
      error,
    } = this.props;

    if (!links) { return <div>Loading</div>; };
    if (error) { return <div>There was an error loading the feed.</div>; };

    return(
      <div className={this.cRoot('mainContainer')} >
        <AddFeedModal
          isVisible={showModal}
          onClose={() => showAddModal(false)}
          onSubmit={(url) => this._handleCreateFeed(url)}
        />
        <div
          className={this.cPrefix('selectFeedBar')}
          >
          <Dropdown
            data={feeds}
            onSelect={(feed) => this._handleSelect(feed)}
            ref='feedDropdown'
            selected={selectedFeed}
          />
          <a
            aria-label='Add'
            onClick={() => showAddModal(true)}
            title={'Click to add a new feed'}>
            +
          </a>
        </div>
        <div className={this.cPrefix('filterFeedsBar')}>
          <FilterLink
            filter='all'
            onClick={() => this._handleSetViewMode('')}
            style={this.cPrefix('showAll')}
            title={'Show All'}>
            All
          </FilterLink>
          <FilterLink
            filter='starred'
            onClick={() => this._handleSetViewMode('SHOW_STARRED')}
            style={this.cPrefix('showStarred')}
            title='Show starred'>
            Starred
          </FilterLink>
        </div>
        <Links
          data={links}
          expanded={isExpanded}
          onClick={(id) => this._handleClick(id)}
          selected={selectedFeed}
          selectedItem={selectedItem}
          viewMode={viewMode}/>
      </div>
    );
  }
};

function mapDispatchToProps(dispatch) {
  return bindActionCreators(actions, dispatch);
}

function mapStateToProps(state) {
  const {
    links,
    feeds,
    selectedFeed,
    selectedItem,
    showModal,
    viewMode,
    setExpanded,
    isExpanded,
    error,
  } = state.feedsReducer;
  return {
    links,
    feeds,
    selectedFeed,
    selectedItem,
    showModal,
    viewMode,
    setExpanded,
    isExpanded,
    error,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
