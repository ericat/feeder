import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';

export default class Dropdown extends React.Component {
  constructor(props) {
    super(props);

    this._renderSelect = this._renderSelect.bind(this);
  }

  _selectFeed(e) {
    this.props.onSelect(e.target.value);
  }

  _renderSelect(values) {
    return values.map((value) =>
      (<option
          key={value._id}
          value={value.name}>{value.name}
        </option>)
    );
  }

  render() {
    return(
      <select
        id='feed'
        title='Selected feed'
        onChange={this._selectFeed.bind(this)}
        ref='feed'
        >
        <option value={''}>{'Select feed'}</option>
        {this._renderSelect(this.props.data)}
      </select>
    );
  }
}

Dropdown.propTypes = {
  data: React.PropTypes.arrayOf(React.PropTypes.shape({
    _id: React.PropTypes.string.isRequired,
    name: React.PropTypes.string.isRequired,
  })).isRequired,
  onSelect: React.PropTypes.func.isRequired,
};
