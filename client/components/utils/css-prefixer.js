function cssPrefixer() {
  return function(component) {
    const prefix = `${component.name}`;

    component.prototype.cPrefix = function(name) {
      return `${prefix}-${name}`;
    };

    component.prototype.cRoot = function(name) {
      return `${prefix} ${name ? prefix + '-' + name : ''} ${this.props.className || ''}` ;
    };
  };
}

module.exports = cssPrefixer;
