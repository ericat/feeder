import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import shallowCompare from 'react-addons-shallow-compare';
import './links.css';

const cssPrefixer = require('../utils/css-prefixer');

@cssPrefixer()
export default class Links extends React.Component {
  constructor(props) {
    super(props);

    this._renderLinks = this._renderLinks.bind(this);
    this._filterVisibleLinks = this._filterVisibleLinks.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    return shallowCompare(this, nextProps, nextState);
  }

  _filterVisibleLinks(links, viewMode, selected) {
    const filtered = links.filter(l => l.feedTitle == selected);

    switch(viewMode) {
      case 'SHOW_SELECTED':
        return filtered;
      default:
        return links;
    }
  }

  _renderLinks(links) {
    const { expanded, selectedItem, onClick } = this.props;
    const parseDate = (date) => new Date(date).toDateString();

    return links.map((link) =>
       (<div
         className={this.cPrefix('clickableItem')}
         key={link._id}
         >
        <div className={this.cPrefix('meta')} >
          <span
            className={this.cPrefix('blog')}>
            {`${link.feedTitle} | `}
          </span>
          <span>{parseDate(link.date)}</span>
        </div>
        <div className={this.cPrefix('body')} >
          <div>
            <a
              className={this.cPrefix('title')}
              onClick={() => onClick(link._id) }
              dangerouslySetInnerHTML={{__html: link.title}}
              title={'Click to expand'}
            />
            <span>{`  ${link.author}`}</span>
          </div>
          {expanded && link._id === selectedItem ?
            <p
              className={this.cPrefix('full')}
              dangerouslySetInnerHTML={{__html: link.content}} /> :
            <p
              className={this.cPrefix('preview')}
              dangerouslySetInnerHTML={{__html: link.preview}}
              onClick={() => onClick(link._id) } />
          }
        </div>
      </div>)
    );
  }

  render() {
    const {
      data,
      viewMode,
      selected,
      onClick
    } = this.props;

    const links = this._filterVisibleLinks(data, viewMode, selected);

    return(
      <div className={this.cRoot('linksContainer')}>
        {this._renderLinks(links)}
      </div>
    );
  }
};

Links.propTypes = {
  data: React.PropTypes.arrayOf(React.PropTypes.shape({
    _id: React.PropTypes.string.isRequired,
    title: React.PropTypes.string.isRequired,
    author: React.PropTypes.string.isRequired,
    content: React.PropTypes.string.isRequired,
    preview: React.PropTypes.string.isRequired,
    url: React.PropTypes.string.isRequired,
    date: React.PropTypes.string.isRequired,
    feedTitle: React.PropTypes.string.isRequired,
    feedUrl: React.PropTypes.string.isRequired,
    starred: React.PropTypes.bool.isRequired,
  })).isRequired,
  expanded: React.PropTypes.bool.isRequired,
  viewMode: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired,
  selectedItem: React.PropTypes.string.isRequired,
};

