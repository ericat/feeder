import 'babel-polyfill';
import React from 'react';
import { render } from 'react-dom';
import './modal.css';

const cssPrefixer = require('../utils/css-prefixer');
const validUrl = require('valid-url');

@cssPrefixer()
export default class AddFeedModal extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      urlValue: '',
      validationError: '',
    }
  }

  _handleChange(e) {
    this.setState({urlValue: e.target.value});
  }

  _handleSubmit(e) {
    e.preventDefault();
    const isValidUrl = validUrl.isHttpUri(this.state.urlValue) ||
      validUrl.isHttpsUri(this.state.urlValue);

      if (!isValidUrl) {
        return this.setState({validationError: 'Not a valid URL'});
      };

    this.props.onSubmit(this.state.urlValue);
  }

  _handleClose() {
    this.setState({validationError: ''});
    this.props.onClose();
  }

  render() {
    const { onClose, errors } = this.props;
    const isVisible = this.props.isVisible ? 'block' : 'none';
    const isError =  errors || this.state.validationError ? 'block' : 'none';

    return (
      <div
        className={this.cRoot('overlay')}
        style={{display: isVisible}}>
        <div
          className={this.cPrefix('modalContainer')}>
          <div
            className={this.cPrefix('body')}>
            <form onSubmit={this._handleSubmit.bind(this)}>
              <label
                id='feed-label'
                for='feed-url'>
                Add feed URL
              </label>
              <input
                aria-labelledby='feed-label'
                aria-required='true'
                id='feed-url'
                onChange={this._handleChange.bind(this)} />
              <p
                className={this.cPrefix('errors')}
                style={{display: isError }}>{errors || this.state.validationError}</p>
              <div
                className={this.cPrefix('actions')}>
                <button
                  className={this.cPrefix('btn')}
                  aria-label="Add"
                  type='submit'
                  onClick={this._handleSubmit.bind(this)}>
                  Add
                </button>
                <a
                  className={this.cPrefix('btn')}
                  aria-label="Close"
                  onClick={() => this._handleClose.bind(this)()}>
                  Close
                </a>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

AddFeedModal.propTypes = {
  errors: React.PropTypes.string,
  isVisible: React.PropTypes.bool.isRequired,
  onSubmit: React.PropTypes.func.isRequired,
  onClose: React.PropTypes.func.isRequired,
};
