import React from 'react';
import { Link } from 'react-router';

const FilterLink = ({
  filter,
  children,
  onClick,
  style,
  title,
}) => {
  return (
    <Link
      activeStyle={{ color: 'red', textDecoration: 'none' }}
      to={filter === 'all' ? '' : filter}
      className={style || ''}
      aria-label={title}
      onClick={() => onClick()}
      title={title}
      >
      {children}
    </Link>
  );
}

FilterLink.propTypes = {
  style: React.PropTypes.string.isRequired,
  onClick: React.PropTypes.func.isRequired,
  title: React.PropTypes.string.isRequired,
};

export default FilterLink;
