var extend = require('util')._extend;
var debug = require('./debug');
var test = require('./test');
var production = require('./production');

if (process.env.NODE_ENV === 'production') {
  module.exports = extend(debug, production);
} else if (process.env.NODE_ENV === 'test') {
  module.exports = test;
} else {
  module.exports = debug;
}
