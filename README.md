To install and run:

```
npm install
npm start
```
To run tests:

```
npm test
npm run test-client
```

Tu run with nodemon:
```
npm run start-forever
```

##### Description
The project displays all feeds initially, but allows filtering by feed name.
The All and Starred links display all items and (all) starred items,
respectively. The article can be expanded by clicking on the title and on the
ellipsis in the preview.

The "plus" button shows a modal that allows the user to add a RSS feed.

![feeder.gif](https://bitbucket.org/repo/aM6dAG/images/3543317058-feeder.gif)

Some of the feed the project has been tested with:

* http://mashable.com/rss/
* http://blog.yld.io/rss/
* https://techcrunch.com/feed/
* http://www.wired.com/rss


##### Mongo shell quick starter
Connect to the database from the mongo shell:

```
mongo
use feeder
show collections // feeds and links
db.feeds.find({}) // find all feeds
```

To cleanup
```
db.feeds.remove({}) // will remove all feeds
```

To drop the database
```
use feeder
db.dropDatabase()
```

To star an item (example):
```
db.links.update({author: "Tim Moynihan"}, {$set:{starred:true}})
```