'use strict';
var request = require('request');
var striptags = require('striptags');
var Feedparser = require('feedparser');
var fs = require('fs');
var path = require('path');

function fetchLinks(url, cb) {
  var links = [];
  var feedparser = new Feedparser();
  var headers = {
    'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/31.0.1650.63 Safari/537.36',
    'accept': 'text/html,application/xhtml+xml'
  };
  var req = request({
    uri: url,
    headers: headers,
    timeout: 20000,
  });

  function done(err) {
    console.error('fetching links error: ', err);
    //fs.writeFileSync('links.json', JSON.stringify(links), {encoding: 'utf8'});
    return cb(err, links);
  }

  req.on('error', done);
  req.on('response', function(res) {
    if (res.statusCode !== 200) {
      throw new Error('Bad status code');
    }
    res.pipe(feedparser);
  });

  feedparser.on('error', done);
  feedparser.on('end', done);
  feedparser.on('readable', () => {
    var item;
    while (item = feedparser.read()) {
      links.push(item);
    }
  });
}

function format(link) {
  var allowedTags = ['div',
    'p',
    'a',
    'blockquote',
    'span',
    'i',
    'strong',
    'pre',
    'h1',
    'h3',
    'h4',
    'h5',
    'section',
    'code'
  ];

  var content = striptags(link.description, allowedTags) || '';
  var truncated = content.substring(0, 100);
  var preview = truncated + '...';

  return {
    title: striptags(link.title) || '',
    author: link.author || '',
    content: content,
    preview: preview,
    date: link.date || link.meta.date || '',
    url: link.link || '',
    feedTitle: link.meta.title || '',
    feedUrl: link.meta.link || '',
  };
}

function parseFeed(links) {
  return links.map(function(link) {
    return format(link);
  });
}

function buildFeedFromMeta(link) {
  return {
    name: link.feedTitle || '',
    url: link.feedUrl || '',
  };
}

module.exports = {
  fetchLinks: fetchLinks,
  parseFeed: parseFeed,
  buildFeedFromMeta: buildFeedFromMeta,
};
