'use strict';
var fs = require('fs');
var path = require('path');
var boom = require('boom');
var validUrl = require('valid-url');
var Feed = require('../../../models').Feed;
var Link = require('../../../models').Link;
var helpers = require('../../helpers');

function _fetchLinksCb(cb) {
  return function(err, links) {
    if (err instanceof Error || links.length === 0) {
      throw err;
    }

    var formattedLinks = helpers.parseFeed(links);
    var feed = helpers.buildFeedFromMeta(formattedLinks[0]);

    Feed.create(feed, _createFeedCb(formattedLinks, cb));
  };
}

function _createFeedCb(formattedLinks, cb) {
  return function(err, savedFeed) {
    if (err) { return cb(err); }
    var length = formattedLinks.length;

    formattedLinks.map(_createLinksCb(length, savedFeed, cb));
  };
}

function _createLinksCb(length, savedFeed, cb) {
  return function(formattedLink) {
    Link.create(formattedLink, function(err, link) {
      if (err) { return cb(err); }

      savedFeed._links.push(link._id);
      length--;

      if (length == 0) {
        return savedFeed.save(_saveFeedCb(savedFeed, cb));
      }
    });
  };
}

function _saveFeedCb(feed, cb) {
  return function(err) {
    if (err) { return cb(err); }
    var selectedProps = ['_id', 'name', 'url'];
    var clone = JSON.parse(JSON.stringify(feed, selectedProps));
    cb(null, clone);
  };
}

module.exports = {
  create: function(body, options, cb) {
    var isValidUrl = validUrl.isHttpUri(body.url) ||
      validUrl.isHttpsUri(body.url);

    if (!isValidUrl) {
      return cb(new Error('Not a valid URL'));
    };

    (options.fetchLinks || helpers.fetchLinks)(body.url, _fetchLinksCb(cb));
  },

  findByName: function(name, cb) {
    Feed
      .findOne({name: name})
      .populate('_links')
        .exec(function(err, feed) {
          if (err || !feed) { return cb(boom.notFound('Not found')); };
          return cb(null, feed._links);
        });
  },

  findNames: function(cb) {
    Feed.find({}, 'name', function(err, feeds) {
      if (err || !feeds) { return cb(boom.notFound('Not found')); };
      return cb(null, feeds);
    });
  },
};
