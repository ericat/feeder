'use strict';
var loadCreate = require('./create');
var loadFindByName = require('./read').loadFindByName;
var loadFindNames = require('./read').loadFindNames;

function loadMethods(server) {
  loadCreate(server);
  loadFindByName(server);
  loadFindNames(server);
}

module.exports = loadMethods;
