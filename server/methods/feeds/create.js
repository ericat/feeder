'use strict';
var feedStore = require('./feed');

function create(feed, options, next) {
  feedStore.create(feed, options, next);
}

function loadCreate(server) {
  server.method('feed.create', create, {});
}

module.exports = loadCreate;
