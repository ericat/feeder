'use strict';
var feedStore = require('./feed');

function findByName(id, next) {
  feedStore.findByName(id, next);
}

function loadFindByName(server) {
  server.method('feed.findByName', findByName, {});
}

function findNames(next) {
  feedStore.findNames(next);
}

function loadFindNames(server) {
  server.method('feed.findNames', findNames, {});
}

module.exports = {
  loadFindByName: loadFindByName,
  loadFindNames: loadFindNames,
};
