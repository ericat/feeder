'use strict';
var loadFindAll = require('./read');
var loadUpsert = require('./upsert');

function loadMethods(server) {
  loadFindAll(server);
  loadUpsert(server);
}

module.exports = loadMethods;
