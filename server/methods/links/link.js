'use strict';
var fs = require('fs');
var path = require('path');
var boom = require('boom');
var Link = require('../../../models').Link;

module.exports = {
  findAll: function(cb) {
    Link.find({}, function(err, links) {
      if (err || !links) { return cb(boom.notFound('Not found')); };
      return cb(null, links);
    });
  },

  upsert: function(id, options, cb) {
    Link.update({_id: id}, options, function(err, query) {
      if (err) { return cb(err); };

      Link.findOne({_id: id}, function(err, link) {
        if (err || !link) { return cb(err); };
        cb(null, link);
      });
    });
  },
};
