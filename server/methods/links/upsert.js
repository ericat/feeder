var linkStore = require('./link');

function upsert(id, options, next) {
  linkStore.upsert(id, options, next);
}

function loadUpsert(server) {
  server.method('link.upsert', upsert, {});
}

module.exports = loadUpsert;
