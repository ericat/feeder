'use strict';

var linkStore = require('./link');

function create(link, next) {
  linkStore.create(link, next);
}

function loadCreate(server) {
  server.method('link.create', create, {});
}

module.exports = loadCreate;
