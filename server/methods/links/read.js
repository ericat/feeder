'use strict';
var linkStore = require('./link');

function findAll(next) {
  linkStore.findAll(next);
}

function loadFindAll(server) {
  server.method('link.findAll', findAll, {});
}

module.exports = loadFindAll;
