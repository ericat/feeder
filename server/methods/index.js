'use strict';
var loadFeedsMethods = require('./feeds');
var loadLinksMethods = require('./links');

function loadMethods(server) {
  loadFeedsMethods(server);
  loadLinksMethods(server);
}

module.exports = loadMethods;
