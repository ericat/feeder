'use strict';
var Hapi = require('hapi');
var pug = require('pug');
var path = require('path');
var vision = require('vision');
var inert = require('inert');
var good = require('good');
var goodConsole = require('good-console');
var mongoose = require('mongoose');
var config = require('../config');
var loadMethods = require('./methods');
var server = module.exports = new Hapi.Server();

server.connection(config.server.connection);

// Error handling
server.ext('onPreResponse', (request, reply) => {
  var response = request.response;
  var statusCode;
  if (!response.isBoom) { return reply.continue(); };
  if (response.output.statusCode === 404) {
    return reply.view('errors', {
      err: response.output.payload}).code(404);
  } else {
    statusCode = response.output.statusCode || 500;
    return reply.view('errors', {
      err: response.output.payload
    }).code(statusCode);
  }
  reply.continue();
});

loadMethods(server);

server.register([vision, inert, {
  register: good,
  options: {
    reporters: {
      console: [{
        module: 'good-squeeze',
        name: 'Squeeze',
        args: [{
          log: '*',
          response: '*'
        }]
      }, {
        module: 'good-console'
      }, 'stdout']
    }
  }
}], function(err) {
  if (err) { throw err; }

  mongoose.connect(config.server.db, function(err) {
    if (err) {
      console.error('Database connection error: ', err);
    }
  });

  server.views({
    engines: {
      pug: pug,
    },
    path: path.join(__dirname, './../views'),
    compileOptions: {
      pretty: true
    },
    relativeTo: __dirname
  });

  server.route({
    method: 'GET',
    path: '/{param*}',
    config: {
      description: 'Static assets',
      handler: {
        directory: {
          path: path.join(__dirname, '../public')
        }
      }
    }
  });

  server.route({
    method: 'GET',
    path: '/',
    handler: function(request, reply) {
      return reply.view('index');
    },
  });

  server.route(require('../routes'));
});
