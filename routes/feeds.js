'use strict';
var joi = require('joi');
var feedsHandlers = require('../handlers/feeds');

var create = {
  method: 'POST',
  path: '/v1/feed',
  config: {
    handler: feedsHandlers.create,
    payload: {
      output: 'data',
    },
    validate: {
      payload: {
        url: joi.string().uri().required(),
      }
    }
  }
};

var findByName = {
  method: 'GET',
  path: '/v1/feeds/{name}',
  config: {
    handler: feedsHandlers.findByName,
    validate: {
      params: {
        name: joi.string().required()
      }
    }
  }
};

var findNames = {
  method: 'GET',
  path: '/v1/feeds/names',
  config: {
    handler: feedsHandlers.findNames,
  }
};

module.exports = [create, findByName, findNames];
