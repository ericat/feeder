'use strict';
var joi = require('joi');
var linksHandlers = require('../handlers/links');

var findAll = {
  method: 'GET',
  path: '/v1/links',
  config: {
    handler: linksHandlers.findAll,
  }
};

var upsert = {
  method: 'PUT',
  path: '/v1/links/{id}',
  config: {
    handler: linksHandlers.upsert,
    validate: {
      params: {
        id: joi.required()
      }
    }
  }
};

module.exports = [findAll, upsert];
